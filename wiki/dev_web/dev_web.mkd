# Installation serveur perso pour développement d'applications PHP/MySql/Html/CSS

## Installation de PHP/MySql

    sudo apt install apache2 php libapache2-mod-php mysql-server php-mysql

## Installation PhpMyadmin

    sudo apt install phpmyadmin

pendant l'installation, il faudra entrer un mot de passe root pour la base de données.
Par défaut, il n'est pas possible de se logger en tant que root sous ubuntu 18.04. On propose donc de créer un utilisateur "antoine" qui aura accès à la base de données.

    sudo mysql

puis 

    GRANT ALL ON *.* TO 'antoine'@'localhost' IDENTIFIED BY 'mot_de_passe_solide' WITH GRANT OPTION;
    FLUSH PRIVILEGES;
    QUIT;

[Plus d'infos](https://doc.ubuntu-fr.org/phpmyadmin)

## Configuration Apache

### UserDir

On utilise le module UserDir qui permet de travailler ces applications php/mysql directement dans le répertoire personnel dans un dossier nommé **public_html**.

Pour cela, on définit l'utilisateur autorisé à utiliser ce module en modifiant le fichier **userdir.conf**.

    sudo nano /etc/apache2/mods-available/userdir.conf 

puis ajouter dans le fichier 

    UserDir enabled antoine

On active ensuite le module puis on relance apache

    sudo a2enmod userdir
    sudo systemctl restart apache2

On créer un dossier public_html dans le dossier perso

    mkdir ~/public_html
    chmod -R 755 ~/public_html

Cela suffit pour réaliser des applications purement en html/css. Si vous créer un fichier index.html dans le dossier **public_html**, il sera visible avec votre navigateur à l'adresse **https://localhost/~antoine/**

### Exécution de PHP dans le répertoire personnel

Cependant pour du PHP/Mysql, il est nécessaire d'autoriser PHP à s'exécuter dans le répertoire personnel. On édite donc un fichier de configuration

    sudo nano /etc/apache2/mods-available/php7.2.conf 

Dans ce fichier, il faut commenter tout le bloc **IfModule**. Ensuite on relance apache une nouvelle fois.

    sudo systemctl restart apache2


### Activer l'affichage des erreurs de code php

Dans le but de faire du développement, il est toujours utile d'avoir des messages d'erreurs qui s'affichent pour corriger le code. Par défaut, les erreurs ne sont pas visibles (page blanche). Pour activer ces messages, il faut modifier le fichier php.ini (chemin retourné par phpinfo()).

    sudo nano /etc/php/7.2/apache2/php.ini 
    
et y modifier valeurs de **display _errors** et **error_reporting**

    display _errors = On 
    error_reporting= E_ALL 