#!/bin/bash
# Script post installation pour Debian 12

apt update -y

echo "================================================="
echo "Choix dépôts et pilotes additionnels"
echo "================================================="
apt install -y software-properties-gtk
software-properties-gtk

echo "================================================="
echo "Mise à jour"
echo "================================================="
apt update -y
apt dist-upgrade -y

echo "================================================="
echo "Bureautique"
echo "================================================="
apt install -y libreoffice libreoffice-l10n-fr
apt install -y calibre keepassxc
apt install -y kmymoney tellico 
#apt install -y freecad librecad
#apt install -y retext
apt install -y mate-calc menulibre

echo "================================================="
echo "Developpement"
echo "================================================="
apt install -y spyder git 
apt install -y geany
#apt install -y bluefish
#apt install -y pyqt5-dev-tools qttools5-dev-tools sqlitebrowser

echo "================================================="
echo "Graphisme"
echo "================================================="
apt install -y pdfarranger simple-scan shotwell
apt install -y imagemagick zenity

echo "================================================="
echo "Internet"
echo "================================================="
apt install -y evolution thunderbird thunderbird-l10n-fr
apt install -y filezilla davfs2
apt install -y gajim hexchat liferea
apt install -y torbrowser-launcher transmission-gtk
#apt install -y whalebird delta-chat
apt install -y chromium chromium-l10n
apt install -y firefox-esr-l10n-fr

echo "================================================="
echo "Multimedia"
echo "================================================="
apt install -y vlc libavcodec-extra
apt install -y ttf-mscorefonts-installer rar unrar gstreamer1.0-libav gstreamer1.0-plugins-ugly gstreamer1.0-vaapi
apt install -y fonts-crosextra-carlito fonts-crosextra-caladea
apt install -y gvfs-backends

echo "================================================="
echo "Utilitaires"
echo "================================================="
###### outils en ligne de commande 
apt install -y htop ncdu screenfetch
###### sensors 
apt install -y lm-sensors
sensors-detect
###### disque dur 
apt install -y gnome-disk-utility 
#apt install -y gparted 
###### imprimante
apt install -y printer-driver-escpr
apt install -y hplip-gui
###### mot de passe
apt install -y keepassxc seahorse

echo "================================================="
echo "Finalisation"
echo "================================================="
apt -y autoremove