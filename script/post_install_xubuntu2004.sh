#!/bin/bash
# Script post installation xubuntu 18.04

echo "================================================="
echo "Choix dépôts et pilotes additionnels"
echo "================================================="
software-properties-gtk

echo "================================================="
echo "Mise à jour"
echo "================================================="
sudo apt -y update
sudo apt -y dist-upgrade

echo "================================================="
echo "Bureautique"
echo "================================================="
sudo apt -y install retext libreoffice
sudo apt -y install calibre keepassxc
sudo apt -y install kmymoney tellico liferea 
sudo apt -y install freecad librecad

echo "================================================="
echo "Multimedia/Internet"
echo "================================================="
sudo apt -y install vlc 
#sudo apt -y install xubuntu-restricted-extras
sudo apt -y install evolution 
sudo apt -y install gajim hexchat

echo "================================================="
echo "Developpement"
echo "================================================="
sudo apt -y install spyder3 git 
sudo apt -y install bluefish filezilla geany
sudo apt -y install pyqt5-dev-tools qttools5-dev-tools sqlitebrowser


echo "================================================="
echo "Utilitaires"
echo "================================================="
###### outils en ligne de commande 
sudo apt -y install htop ncdu screenfetch
###### sensors 
sudo apt -y install lm-sensors
sudo sensors-detect
###### disque dur 
sudo apt -y install gnome-disk-utility 
sudo apt -y install gparted

sudo apt -y install seahorse printer-driver-escpr

#sudo apt -y install virtualbox   usb-creator-gtk
sudo apt -y install keepassxc kazam unison-gtk

sudo apt -y remove snapd

echo "================================================="
echo "Francisation du système"
echo "================================================="
gnome-language-selector
