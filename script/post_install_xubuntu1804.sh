#!/bin/bash
# Script post installation xubuntu 18.04

echo "================================================="
echo "Choix dépôts et pilotes additionnels"
echo "================================================="
software-properties-gtk

echo "================================================="
echo "Mise à jour"
echo "================================================="
sudo apt -y update
sudo apt -y dist-upgrade

echo "================================================="
echo "Bureautique"
echo "================================================="
sudo apt -y install retext libreoffice
#sudo apt -y install calibre 
#sudo apt -y install kmymoney tellico liferea  planner 

echo "================================================="
echo "Multimedia"
echo "================================================="
sudo apt -y install vlc 
#sudo apt -y install xubuntu-restricted-extras
sudo apt -y install evolution pidgin
sudo apt -y remove thunderbird
#sudo apt -y install winff cheese pinta

echo "================================================="
echo "Developpement"
echo "================================================="
sudo apt -y install spyder3 git 
#sudo apt -y install poedit bluefish filezilla guake

echo "================================================="
echo "Mises à jour automatique"
echo "================================================="
sudo apt -y install unattended-upgrades # à configurer
echo "==> Configuration des paquets à mettre à jour automatiquement"
sudo nano /etc/apt/apt.conf.d/50unattended-upgrades
echo "==> Configuration de la fréquence de la mise à jour"
sudo nano /etc/apt/apt.conf.d/20auto-upgrades 

echo "================================================="
echo "Utilitaires"
echo "================================================="
###### outils en ligne de commande 
sudo apt -y install htop ncdu screenfetch
###### sensors 
sudo apt -y install lm-sensors
sudo sensors-detect
###### disque dur 
sudo apt -y install gnome-disk-utility 
#sudo apt -y install gparted

#sudo apt -y install virtualbox kazam unison-gtk 
sudo apt -y install keepassxc usb-creator-gtk

echo "================================================="
echo "Services en ligne"
echo "================================================="
###### dropbox 
#sudo apt -y install thunar-dropbox-plugin
###### teamviewer
#sudo apt -y install gdebi-core
#wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
#sudo gdebi teamviewer_amd64.deb

echo "================================================="
echo "Francisation du système"
echo "================================================="
gnome-language-selector