Title: Astuce Latex : Texdoc  
Date: 2011-11-05 17:20  
Category: Bureautique  
Tags:  

Slug: astuce-latex-texdoc  
Status: published  

Petite astuce utile pour les utilisateurs de Latex trouvée sur le forum [ubuntu-fr.org](http://forum.ubuntu-fr.org/viewtopic.php?pid=6724391#p6724391)

[texdoc](http://www.tug.org/texdoc/) est un petit programme qui permet de trouver et lire la documentation de Tex Live. C'est en quelque sorte le **man** pour Latex.

Par exemple un

`texdoc beamer`

vous affichera la documentation de beamer.

Une foule d'option est disponible notamment pour rechercher la documentation.

    $ texdoc
    texdoc tries to find appropriate TeX documentation for the specified NAME(s).
    With no NAME, it can print configuration information (-f, --files);
    the usual --help and --version options are also accepted.
    Usage: texdoc [OPTIONS]... [NAME]...
      -f, --files           Print the name of the config files being used.
      -w, --view            Use view mode: start a viewer.
      -m, --mixed           Use mixed mode (view or list).
      -l, --list            Use list mode: show a list of results.
      -s, --showall         Use showall mode: show also "bad" results.
      -r, --regex           Use regex mode. (Deprecated.)
      -e, --extensions=L    Set ext_list=L. (Deprecated.)
      -a, --alias           Use the alias table.
      -A, --noalias         Don't use the alias table.
      -i, --interact        Use interactive menus.
      -I, --nointeract      Use plain lists, no interaction required.
      -v, --verbosity=N     Set verbosity level to N.
      -d, --debug[=list]    Activate debug for selected items (default all).
      -M, --machine         Use a more machine-friendly output format.
    Environment: PAGER, BROWSER, PDFVIEWER, PSVIEWER, DVIVIEWER.
    Files: /texdoc/texdoc.cnf files, see the -f option.
    Homepage: http://tug.org/texdoc/Manual: displayed by `texdoc texdoc'.

pour ceux qui veulent plus d'infos

`texdoc texdoc`

</p>

