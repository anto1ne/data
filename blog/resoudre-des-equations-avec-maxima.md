Title: résoudre des équations avec Maxima  
Date: 2009-10-08 18:12  
Category: Calcul  
Tags:  

Slug: resoudre-des-equations-avec-maxima  
Status: published  

![wxmaxima](./public/2009/maxima.png "wxmaxima, oct. 2009")

Présentation
------------

wxMaxima est un logiciel de calcul formel.

C'est un équivalent gratuit des logiciels Maple et Mathematica.

Avec ce logiciel, il est possible de réaliser de nombreuses opérations mathématiques : intégrales, dérivées, calcul matriciels, séries, limites , résolutions d'équations.

Exemples pratiques
------------------

Par exemple pour connaitre les racines d'un polynôme du second degré :

`solve([a*x*x + b*x + c = o], [x]);`

pour représenter la fonction z = sin(x) + sin(y)

`wxplot3d(sin(x) + cos(y), [x,-5,5], [y,-5,5]);`

Liens
-----

[Le site de ce logiciel](http://michel.gosse.free.fr/) comprend un forum ainsi qu'une [documentation](http://michel.gosse.free.fr/documentation/index.html) bien fournie.

</p>

