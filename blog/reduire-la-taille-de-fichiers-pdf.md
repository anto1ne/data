Title: Réduire la taille de fichiers PDF  
Date: 2010-03-19 18:01  
Category: Bureautique  
Tags:  

Slug: reduire-la-taille-de-fichiers-pdf  
Status: published  

## Petite astuce trouvée sur [ubuntu-fr.org](http://forum.ubuntu-fr.org/viewtopic.php?id=384167) pour réduire facilement la taille de fichiers PDF

    gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -sOutputFile=fichier_reduit.pdf fichier_a_reduire.pdf

## Petit test maison sur un rapport contenant des images pas forcément comprimées avant insertion.

J'ai fait un test sur un fichier de 2760 Ko, une fois cette commande utilisée, il passe à 752 Ko :)

Sur l'écran comme à l'impression, il n'y pas de différence visible.

</p>

