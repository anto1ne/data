Title: Nouveau pc :-) , le tout sous Gnu/Linux  
Date: 2011-10-01 16:41  
Category: Matériel  
Tags:  

Slug: nouveau-pc-le-tout-sous-gnulinux  
Status: published  

Je vous présente ma nouvelle config que je viens d'acquérir. En effet mon [pc portable HP](/index.php?post/2009/10/02/Ubuntu-sur-HP-6830s-reconnaissance-mat%C3%A9riel) m'a déjà lâché au bout de deux ans :-( .En ayant un peu marre des cartes mères qui lâche sur les portables, je me suis lancé dans l'achat d'un pc fixe sans OS et qui je l'espère tiendra le coup plus longtemps. Je recherchais un matériel principalement orienté vers la bureautique, internet avec la possibilité de faire des petits jeux de temps en temps, le tout compatible Gnu/Linux.

Voilà un petit récapitulatif du matériel.

Le boîtier
----------

Le boîtier est un [Fractal Design Define R3 - Silver Arrow](./public/2011/pc2011/Define_R3_Sales_Sheet_english.pdf) . Rien de spécial à signaler c'est un bon boîtier bien refroidie.

![Fractal\_Design\_Define\_R3\_-\_Silver\_Arrow\_2.jpeg](./public/2011/pc2011/.Fractal_Design_Define_R3_-_Silver_Arrow_2_m.jpg "Fractal_Design_Define_R3_-_Silver_Arrow_2.jpeg, oct. 2011")

L'alimentation
--------------

Une alimentation 520 W de Antec.

![Antec\_HCG\_-\_520W.jpg](./public/2011/pc2011/Antec_HCG_-_520W.jpg "Antec_HCG_-_520W.jpg, oct. 2011")

La carte mère
-------------

Je suis un peu déçu pour l’instant de la Gigabyte GA-970A-UD3. Elle fait bien son boulot.

![Gigabyte\_GA-970A-UD3.jpg](./public/2011/pc2011/Gigabyte_GA-970A-UD3.jpg "Gigabyte_GA-970A-UD3.jpg, oct. 2011")

Mais la partie audio a un petit bug. Lorsque l'on branche un casque audio, le son est bien présent dans le casque mais n'est pas coupé au niveau des hauts parleurs. Pour info lspci me renvoie

`$ lspci | grep Audio`  
`00:14.2 Audio device: ATI Technologies Inc SBx00 Azalia (Intel HDA) (rev 40)`  
`01:00.1 Audio device: nVidia Corporation GF108 High Definition Audio Controller (rev a1)`  

Le processeur
-------------

Le processeur est un [AMD Phenom II X4 965 Black Edition](./public/2011/pc2011/proc.pdf), 4 cœurs donc cadencés à 3.20 GHz. Rien à signaler.

![AMD\_Phenom\_\_II\_X4\_965\_Black\_Edition\_\_125W\_.jpg](./public/2011/pc2011/AMD_Phenom__II_X4_965_Black_Edition__125W_.jpg "AMD_Phenom__II_X4_965_Black_Edition__125W_.jpg, oct. 2011")

Le ventirad
-----------

Le ventirad est un [Cooler Master Hyper TX3](./public/2011/pc2011/HyperTX3_ProductSheet.pdf). Bien plus gros que je m'y attendais en voyant l'image, il fait bien son boulot, pas facile de faire monter le processeur en température.

![Cooler\_Master\_Hyper\_TX3.jpg](./public/2011/pc2011/Cooler_Master_Hyper_TX3.jpg "Cooler_Master_Hyper_TX3.jpg, oct. 2011")

La carte graphique
------------------

La carte graphique MSI N440GT-MD1GD3/LP a été choisi pour son chipset Nvidia GeForce GT 440 qui ne pose pas de problème de reconnaissance sous Gnu/Linux.Elle est largement suffisante pour les petits jeux auxquels je joue, la qualité de l'image me change de ce que j'avais l'habitude de voir auparavant.

![MSI\_N440GT-MD1GD3\_LP.jpg](./public/2011/pc2011/MSI_N440GT-MD1GD3_LP.jpg "MSI_N440GT-MD1GD3_LP.jpg, oct. 2011")

L'écran
-------

L'écran 22"[Iiyama ProLite E2208HDS-B2](./public/2011/pc2011/prolite_e2208hds-2_1317241234.pdf) remplis bien son rôle, pas de pixel mort. Des hauts parleurs sont présents, pas très puissant c'est suffisant pour de la bureautique.

![Iiyama\_ProLite\_E2208HDS-B2.jpg](./public/2011/pc2011/Iiyama_ProLite_E2208HDS-B2.jpg "Iiyama_ProLite_E2208HDS-B2.jpg, oct. 2011")

Mémoire vive
------------

J'ai choisis la ram G.Skill Kit Extreme3 2 x 2 Go PC10600 Ripjaws CAS 9 qui suffit pour mes besoins de bureautique.

![G.Skill\_Kit\_Extreme3\_2\_x\_2\_Go\_PC10600\_Ripjaws\_CAS\_9.jpg](./public/2011/pc2011/G.Skill_Kit_Extreme3_2_x_2_Go_PC10600_Ripjaws_CAS_9.jpg "G.Skill_Kit_Extreme3_2_x_2_Go_PC10600_Ripjaws_CAS_9.jpg, oct. 2011")

Le disque dur
-------------

Le disque dur est un Western Digital Caviar Black SATA Revision 3.0 - 500 Go - 32 Mo, rien à redire, il est silencieux avec une capacité qui est largement suffisante pour moi.

![Western\_Digital\_Caviar\_Black\_SATA\_Revision\_3.0\_-\_500\_Go\_-\_32\_Mo.jpg](./public/2011/pc2011/Western_Digital_Caviar_Black_SATA_Revision_3.0_-_500_Go_-_32_Mo.jpg "Western_Digital_Caviar_Black_SATA_Revision_3.0_-_500_Go_-_32_Mo.jpg, oct. 2011")

Le lecteur- graveur DVD
-----------------------

Le Lite-On IHAS124 - OEM fait bien son boulot, il est un peu bruyant mais je l'utilise très rarement.

![Lite-On\_IHAS124\_-\_OEM.jpg](./public/2011/pc2011/Lite-On_IHAS124_-_OEM.jpg "Lite-On_IHAS124_-_OEM.jpg, oct. 2011")

Souris et Clavier
-----------------

La souris est une Logitech LS1 Laser Mouse (grape acid flash), souris basique avec une petite surprise, les clics gauche et droite de la roulette, je ne connaissais pas, je n'en ai pas trouvé l'utilité pour l'instant non plus.

![Logitech\_LS1\_Laser\_Mouse\_\_grape\_acid\_flash\_.jpg](./public/2011/pc2011/Logitech_LS1_Laser_Mouse__grape_acid_flash_.jpg "Logitech_LS1_Laser_Mouse__grape_acid_flash_.jpg, oct. 2011")

Le clavier Logitech Ultra Flat Keyboard n'a rien d'originale, joli look avec un agencement des touches un peu spécial (on n'aime ou on n'aime pas). Je suis tout de même un peu déçu de ce produit Logitech qui a des touches que je trouve bruyante.

![Logitech\_Ultra\_Flat\_Keyboard\_2.jpg](./public/2011/pc2011/Logitech_Ultra_Flat_Keyboard_2.jpg "Logitech_Ultra_Flat_Keyboard_2.jpg, oct. 2011")

Bilan
-----

Au final le [pc](./public/2011/pc2011/pc.html) fonctionne bien sous Gnu/Linux (testé sous Xubuntu 11.04 pour l'instant, [lshw par ici](./public/2011/pc2011/lshw.html)) et correspond bien à mes besoins.Pour les allergiques au bruit, j'ai un léger bruit de fond de ventilo en permanence en usage bureautique. Par contre lorsque je lance un jeu l'augmentation de la vitesse des ventilos ne se fait pas du tout ressentir.

</p>

