Title: Sortie de emesene 1.6 client msn sous Linux  
Date: 2010-01-10 15:27  
Category: Communication  
Tags:  

Slug: sortie-de-emesene-16-client-msn-sous-linux  
Status: published  

![emesene\_logo](./public/2010/Emesene-logo.png "emesene_logo, janv. 2010")

emesene est un logiciel de messagerie instantanée pour le protocole msn qui fonctionne sur Gnu/Linux.

En ce début d'année 2010, une nouvelle version stable vient de sortir la 1.6.

Dans cette nouvelle version, on pourra remarquer

-   une amélioration de l'ergonomie
-   une nouvelle fenêtre de préférence (qui regroupe la fenêtre des plugins maintenant)

Pour les utilisateurs de Ubuntu, cette dernière version est disponible via [getdeb](./ajout-des-depots-getdeb-sous-ubuntu-910.md), les archives sont également [disponibles](http://www.emesene.org/download.html) pour les autres distributions.

Liens
-----

[Site officiel emesene](http://emesene.org/)  
[Annonce de la nouvelle version](http://emesene-msn.blogspot.com/2010/01/emesene-16-mate-has-been-released.html)  
[Getdeb](http://www.getdeb.net/welcome/)  
[emesene sur Wikipedia](http://fr.wikipedia.org/wiki/Emesene)

</p>

