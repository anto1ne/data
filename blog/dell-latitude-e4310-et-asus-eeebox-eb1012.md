Title: Dell Latitude E4310 et Asus Eeebox EB1012  
Date: 2018-10-28 15:21  
Category: Matériel  
Tags:  

Slug: dell-latitude-e4310-et-asus-eeebox-eb1012  
Status: published  

Nouveau pc !! Voilà un petit feedback concernant deux pc que je viens d'acheter.

Dell E4310
----------

![dell\_e4310.jpg](./public/2018/novembre/dell_e4310.jpg "dell_e4310.jpg, oct. 2018")

Le premier est un Dell Latitude E4310 acheté reconditionné sur le net. Il a les caractéristiques suivantes :

-   Processeur : Intel Core i5 2.4 GHz
-   Mémoire vive : 4 GB
-   Écran : 13,3 pouces
-   Wifi : Intel Centrino Ultimate-N 6300
-   Carte Graphique : Intégrée - Intel Graphics Media Accelerator (Gma) 4500Mhd
-   Webcam et microphone intégrée

Plus de détails dans la [doc technique.](./public/2018/novembre/dell_e4310.pdf)

Avant d'y installer Xubuntu 18.04, j'ai remplacé le disque dur mécanique par un SSD et mis à jour le Bios (version A15). L'installation s'est passée sans difficulté particulière. Le wifi fonctionne "out of the box" et pas besoin de driver pour la carte graphique.

Le tout est assez rapide (merci le SSD) et vraiment idéal pour se déplacer. L'ordinateur rentre facilement dans un sac à dos et est vraiment passe partout. Cela reste du modèle d'occasion et le ventilateur se fait de temps en temps entendre et l'autonomie de la batterie est d'environ 1h30. Son seul défaut à mais yeux reste son unique port usb.

Je compte principalement m’en servir pour de la bureautique et de la programmation.

Asus Eeebox EB1012
------------------

![eeebox.jpeg](./public/2018/novembre/eeebox.jpeg "eeebox.jpeg, oct. 2018")

Le second est une Asus Eeebox EB1012 acheté via leboncoin. Sa config est la suivante :

-   Intel Atom 330 / 1.6 GHz
-   2 Go de ram DDR2
-   HDD 250 GB 5400 rpm

J'y ai installé dans un premier temps une xubuntu 18.04, l'ensemble est utilisable bien que l'on sent un manque de réactivité notamment lorsque l'on a plusieurs onglets avec une vidéo ouverts sur firefox. Cette config est, en effet, plus légère et le disque dur à 5400 rpm n'arrange pas les choses. Pas d'incompatibilité matériel à signaler.

Je viens d'y installer [Yunohost](https://yunohost.org/) pour découvrir l’auto-hébergement et il fait très bien l'affaire. Cet ordinateur est très silencieux et économe en énergie, son seul défaut est selon moi son antenne wifi qui ne se dévisse pas et qui risque de facilement se casser lors d'une mauvaise manipulation.

</p>

