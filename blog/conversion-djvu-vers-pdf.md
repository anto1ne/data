Title: Conversion .djvu vers .pdf  
Date: 2010-10-28 19:26  
Category: Bureautique  
Tags:  

Slug: conversion-djvu-vers-pdf  
Status: published  

Astuces pour les personnes ayant des fichiers .djvu et souhaitant les convertir en .pdf

1.  installer DJview4 (sudo apt-get install djview4)
2.  ouvrir le .djvu avec djview4
3.  Menu : Fichier &gt; Exporter : choisir le format .pdf

Chez moi le fichier .djvu faisait 5.6Mo, après conversion le pdf pesait 203 Mo ... :(J'ai alors procéder à [une réduction du poids du pdf](./reduire-la-taille-de-fichiers-pdf).J'ai obtenu au final un pdf de 170 Mo, toujours pas terrible mais c'est déjà ça :)

Astuce trouvée sur [ubuntuforums.org](http://art.ubuntuforums.org/showthread.php?t=1232038)

</p>

