Title: Clé USB Wifi Netgear WG111  
Date: 2011-07-30 13:59  
Category: Matériel  
Tags:  

Slug: cle-usb-wifi-netgear-wg111  
Status: published  

![sds](./public/2011/wg111v2.jpg "wg111v2, juil. 2011")

Je vous présente cette clé qui fonctionne directement sous toutes les distributions Gnu/Linux que j'ai testées jusqu'ici.

Un lsusb renvoie cette information

`Bus 001 Device 005: ID 0846:4260 NetGear, Inc. WG111v3 54 Mbps Wireless [realtek RTL8187B]`

Des informations complémentaires sont accessibles sur la [documentation de la clé](./public/2011/wg111v3_fr.pdf).

</p>

