Title: Installer Gajim-svn  
Date: 2009-10-13 20:01  
Category: Communication  
Tags:  

Slug: installer-gajim-svn  
Status: published  

![gajim\_logo](./public/2009/gajim_logo.png "gajim_logo, oct. 2009")

Présentation
------------

Gajim est un client de messagerie instantanée qui supporte le protocole Jabber.

Je vais décrire ici comment installer la version svn de Gajim sur Ubuntu.

<s>Le terme "svn" signifie que le logiciel est en faite en développement et donc pas parfaitement stable. Par conséquent c'est une version plus récente mais qui peut contenir des bugs.</s>

edit &gt; erreur de ma part, pour "svn" , [une description de ce terme sur wikipedia](http://fr.wikipedia.org/wiki/Subversion_(logiciel)), merci à zobi8225

Installation
------------

Tout d'abord il faut supprimer toute ancienne version de Gajim de votre système pour éviter tout conflit.

`sudo apt-get remove gajim`

Puis ajouter le dépôt de la version svn de Gajim dans le source.list

`sudo nano /etc/apt/source.list`

ajouter ensuite en bas du fichier

`deb ftp://ftp.gajim.org/debian unstable main`

enregistrer Ctrl + opuis quitter Ctrl + x

enfin passez à l'installation du logiciel

`sudo apt-get updatesudo apt-get install gajim-dev-keyring`

vous devrez confirmer l’installation de ce premier paquet pour pouvoir terminer avec

`sudo apt-get install gajim-svn`

Source
------

<http://www.gajim.org/>

</p>

