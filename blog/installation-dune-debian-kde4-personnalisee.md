Title: Installation d'une Debian Kde4 personnalisée  
Date: 2009-11-06 17:10  
Category: Distribution  
Tags:  

Slug: installation-dune-debian-kde4-personnalisee  
Status: published  

![debian](./public/2009/debian.jpg "debian, nov. 2009")

Je présente ici comment installer une debian avec Kde4 à partir du cd de netinstall en y ajoutant sa propre personnalisation.

Télécharger le cd puis gravure
------------------------------

pour télécharger le cd

    wget -c http://cdimage.debian.org/debian-cd/5.0.3/i386/iso-cd/debian-503-i386-netinst.iso

il ne reste plus qu'à graver ;)

Installation
------------

Je ne vais pas décrire l'installation dans sa totalité.

Procéder à l'installation en suivant les différentes étapes de l'installateur.

Pour avoir une installation dite "minimal" (c'est à dire sans environnement de bureau; juste le stricte minimum pour faire fonctionner le pc), il faut faire très attention à **décocher la case "Desktop environnement"** dans l'étape "Sélection de paquets logiciels".

Terminer l'installation puis redémarrer.

Vous pouvez maintenant vous logger sur votre pc, le tout en ligne de commande.

### Personnalisation du sources.list

L'étape suivante consiste à modifier le sources.list qui permet de configurer les dépôts, là où sont tous les programmes disponibles en téléchargement et prêt à être installés.

Pour modifier ce fichier, taper

`su - nano /etc/apt/sources.list`

Mon choix s'est tourné vers un mix entre la version unstable et testing.Voici à quoi ressemble mon fichier :

    deb http://ftp.fr.debian.org/debian/ testing main contrib non-free  
    #deb-src http://ftp.fr.debian.org/debian/ testing main contrib non-free
    
    deb http://security.debian.org/ testing/updates main contrib non-free  
    #deb-src http://security.debian.org/ testing/updates main contrib non-free
    
    deb http://ftp.fr.debian.org/debian/ unstable main contrib non-free  
    #deb-src http://ftp.fr.debian.org/debian/ testing main contrib non-free

Ne pas oublier d'enregistrer avant de quitter.

### Mise à jour

La mise à jour se fait simplement

`su - apt-get updateapt-get upgradeapt-get dist-upgrade`

Vous avez maintenant une debian toute fraiche prête à être personnalisée. :D

Création de son environnement personnalisé
------------------------------------------

### Environnement de bureau

Mon choix a été d'installer Kde4 dans sa version minimal

`apt-get install kde-minimal kde-l10n-fr`

### Multimédia

J'ai installé le bon Vlc et Kmix pour régler facilement le volume

`apt-get install vlc kmix`

Remarque : à la première ouverture de session de Kde j'ai remarqué que je n'avais pas de son, pour régler cela aller dans Kmix et vérifier que le son n'est pas sur muet pour le Master et le PCM.

### Internet

J'ai installé firefox et thunderbird version debian ainsi que Kopete,akregator et flash.

    apt-get install iceweasel icedove iceweasel-l10n-fr icedove-l10n-fr kopete akregator flashplugin-non-free

### Quelques outils

`apt-get install kate ark okular`

Intégration de Firefox et Thunderbird dans Kde4
-----------------------------------------------

Une petite astuce pour que les deux applications s'intègrent mieux au look de Kde4

`apt-get install gtk2-engines-qtcurve   gtk-qt-engine`

Pour avoir thunderbird et firefox comme application par défaut à la place de Konqueror et Kmail

Taper cette commande puis choisir Iceweasel (firefox)

`su -update-alternatives --config x-www-browser`

Il faut également aller dans

    K>configuration du système >application par défaut

puis changer les valeurs pour le navigateur internet le le courriel.

Un petit aperçu ;)
------------------

Le résultat me donne un système d'environ 1,7Go.

Je n'ai pas beaucoup utilisé cette distribution pour l'instant mais **le choix d'un mélange de testing et unstable n'était pas vraiment judicieux**. J'ai remarqué quelques bugs, mais ils ne sont pas génant au point de rendre la distribution inutilisable.

![debian\_perso](./public/2009/debian_perso.jpg "debian_perso, nov. 2009")

</p>

