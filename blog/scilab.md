Title: Scilab  
Date: 2009-10-13 17:21  
Category: Calcul  
Tags:  

Slug: scilab  
Status: published  

![scilab](./public/2009/scilab.png "scilab, oct. 2009")

Scilab est un logiciel de calcul numérique. Il est très utilisé dans la communauté scientifique.

C'est un équivalent du logiciel propriétaire Matlab.

Scilab permet d'effectuer des calculs de tous genres, de traiter des données expérimentales et de les afficher.

![scilab](./public/2009/scilab1.png "scilab, oct. 2009")

Liens
-----

[Scilab](http://www.scilab.org/)

[Scilab sur wikipedia](http://fr.wikipedia.org/wiki/Scilab)

</p>

