Title: Ajout des dépôts GetDeb sous Ubuntu 9.10  
Date: 2009-11-12 20:04  
Category: Dépôt  
Tags:  

Slug: ajout-des-depots-getdeb-sous-ubuntu-910  
Status: published

GetDeb est un projet dont l'objectif est de fournir les versions les plus récentes de nombreuses applications pour la distribution Ubuntu.

Dépôt GetDeb
------------

Pour cela, ce projet met à disposition des dépôts à ajouter aux sources.list.

`deb http://archive.getdeb.net/ubuntu karmic-getdeb apps games`

il est ensuite nécessaire d'installer la clé

`wget -O- http://archive.getdeb.net/getdeb-archive.key | sudo apt-key add -`

Ce dépôt fournit un grand nombre d'applications directement accessible avec synaptic ou apt.

Retour d'expérience personnel
-----------------------------

Par exemple, j'ai installé **Homebank** et **enemy-territory**.

L'utilisation de ce dépôt permet d'avoir la version 4.1 de Homebank au lieu de la version 4.0.3 du dépôt officiel, rien d'impressionnant donc juste quelques corrections de bugs.

    homebank:  
    Installé : 4.1-1[]{}getdeb2  
    Table de version :  
    \*\*\* **4.1-1\~getdeb2 0**  
    500 http://archive.getdeb.net karmic-getdeb/apps Packages  
    100 /var/lib/dpkg/status  
    **4.0.3-1 0**  
    500 http://fr.archive.ubuntu.com karmic/universe Packages
    
Pour enemy-territory, il faut reconnaitre que l'installation depuis un dépôt est bien plus simple qu'une installation manuelle.

Cependant sans y avoir trop jouer je pense que je vais rencontrer [les mêmes problèmes qu'avec la version installée manuellement](./wolfenstein-enemy-territory-sous-ubuntu-904). Par exemple, le son ne fonctionne pas directement. :(

Liens
-----

[Homebank](http://homebank.free.fr/)  
[GetDeb](http://www.getdeb.net/welcome/)  
[PlayDeb](http://www.playdeb.net/welcome/)  
[GetDeb sur Ubuntu-fr](http://doc.ubuntu-fr.org/getdeb)

</p>

