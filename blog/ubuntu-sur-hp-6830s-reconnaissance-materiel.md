Title: Ubuntu sur HP 6830s & reconnaissance matériel  
Date: 2009-10-02 18:44  
Category: Matériel  
Tags:  

Slug: ubuntu-sur-hp-6830s-reconnaissance-materiel  
Status: published  

![HP\_6830s](./public/2009/HP6830S.jpg "HP_6830s, oct. 2009")

Un retour d'expérience pour ceux qui possèdent un pc portable HP 6830s

J'y ai installé à la fois Ubuntu puis Kubuntu 9.04.

Wifi
----

    03:00.0 Network controller: Intel Corporation PRO/Wireless 5100 AGN Shiloh Network Connection

La carte wifi est reconnu directement, juste à rentrer la clé wep et voilà internet :) .Un bouton intégré au portable permet de désactiver/activer la carte wifi. Ce bouton est bien reconnu.

Ethernet
--------

    86:00.0 Ethernet controller: Marvell Technology Group Ltd. Device 436c

Je n'ai pas jusqu'ici utilisé ce matériel. D'après des sources, celle ci fonctionne bien

Son
---

    00:1b.0 Audio device: Intel Corporation 82801I (ICH9 Family) HD Audio Controller (rev 03)

Après l'installation par défaut le son ne fonctionne pas dans les hauts parleurs mais uniquement dans les écouteurs :( .

Je vous donne ici le moyen de résoudre ce problème.À l'aide de la console on crée/modifie le fichier

`sudo nano /etc/modprobe.conf`

à la fin de ce fichier on ajoute

    options snd-hda-intel model=laptop

pour les non amateurs de nano Ctrl+o puis Ctrl+x pour enregistrer puis fermer le fichier enfin on relance le son à l'aide de

`sudo alsa force-reload`

une fois passé cette étape, le son fonctionnera. La dernière commande devrai être relancée si vous n'avez pas de son a l'ouverture de session (et ainsi ne pas avoir besoin e redémarrer).

Carte graphique ATI Mobility Radeon HD 3430
-------------------------------------------

    01:00.0 VGA compatible controller: ATI Technologies Inc Device 95c2

L'écran est tout de suite dans de bonne résolution. L'installation du pilote propriétaire par le [gestionnaire de pilotes proprietaires](http://doc.ubuntu-fr.org/gestionnaire_de_pilotes_proprietaires) est la méthode la plus rapide.

Webcam intégrée
---------------

> Bus 001 Device 002: ID 04f2:b083 Chicony Electronics Co., Ltd

Elle est directement reconnu, prête à l'emploi :) .

Ce qui marche ... ou pas
------------------------

Les touches Fn sont partiellement reconnu, je crois quelles sont mieux reconnues sous Ubuntu que Kubuntu notamment pour la touche "Imprim écran" . Pourquoi ?

Les ports usb, le lecteur cd le touchpad ne m'ont pas causés de problèmes particuliers jusqu'ici.Je n'ai pas testé le lecteur de carte SD et le modem.

Impression perso
----------------

Je suis pas trop mécontent de ce pc. Pas trop de bidouilles nécessaires pour avoir quelque chose qui fonctionne. Bref un pc prêt à l'emploi après installation.La batterie a une autonomie de 2h30- 3h ce qui est appréciable.

Je reste cependant déçu des ventilateurs qui sont bruyant et qui tourne sans trop de raison, le pc est très peu sollicité. Une gestion de l'alimentation en Powersave permet de légère amélioration.

Sources
-------

<http://www.linlap.com/wiki/hp-compaq+6830s>

</p>

