Title: Calcul de structures avec pyBar  
Date: 2009-10-16 22:03  
Category: Mécanique  
Tags:  

Slug: calcul-de-structures-avec-pybar  
Status: published  

![pyBar](./public/2009/pyBar.png "pyBar, oct. 2009")

pyBar est un logiciel de calcul de structures RDM (Résistance Des Matériaux). Il est orienté vers le calcul de structure de type poutre.

Avec ce logiciel, il est possible der rapidement calculé la déformée d'une poutre, les chargements et réactions d'un système.

pyBar est écrit en python, ce qui en fait un logiciel multiplateforme.

Prérequis
---------

Vérifier que ces paquets soient bien installés

-   python
-   pygtk
-   numpy

Installation
------------

Téléchargement de l'archive d'installation

`wget http://open.btp.free.fr/download/pyBar2.1.tar.gz`

on crée un dossier pour décompresser l'archive

`mkdir pyBar`

on décompresse cette archive

`tar -xvzf pyBar2.1.tar.gz -C  pyBar/`

on la déplace dans le dossier /opt/

`sudo mv pyBar/ /opt/`

il est maintenant possible de lancer pyBar avec cette commande

`/opt/pyBar/pyBar`

on peut aussi utiliser cette dernière commande pour créer un raccourci vers pyBar ;)

Liens
-----

[Lien pyBar](http://open.btp.free.fr/?/pyBar/)

</p>

