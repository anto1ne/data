Title: Convertir base de données Access mdb en csv  
Date: 2011-12-04 21:51  
Category: Bureautique  
Tags:  

Slug: convertir-base-de-donnees-access-mdb-en-csv  
Status: published  

Petit mémo pour convertir des bases de données .mdb en csv.

Il faut utiliser l'outil [mdbtools](http://mdbtools.sourceforge.net/).

Tout d'abord on liste les tables présentes dans le fichier mdb

`mdb-tables bdd.mdb`

ensuite pour récupérer la table qui nous intéresse dans un fichier csv

`mdb-export bdd.mdb ma_table >> ma_table.csv`

[Sources](http://nialldonegan.me/2007/03/10/converting-microsoft-access-mdb-into-csv-or-mysql-in-linux/)

</p>

