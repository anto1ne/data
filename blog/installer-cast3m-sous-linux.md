Title: Installer CAST3M sous Linux  
Date: 2009-10-07 17:36  
Category: Calcul  
Tags:  

Slug: installer-cast3m-sous-linux  
Status: published  

![castem](./public/2009/castem.png "castem, oct. 2009")

Castem est un logiciel de calcul de structures pas éléments finis.

Il utilise le langage GIBIANE. Castem utilise les fichiers de type fichier**.dgibi**

Téléchargement
--------------

Pour installer Castem sous linux, il faut tout d'abord le télécharger.

Pour cela, il faut se rendre sur ce lien <http://www-cast3m.cea.fr/cast3m/downloadsubreq.do> pour accepter la licence.

Il est ensuite possible de télécharger l'archive **Cast3M-2009-linux32.tar.gz**

Installation
------------

`sudo apt-get install libg2c0 zsh`

Placez l'archive que vous avez téléchargée dans votre répertoire personnel.

Pour le vérifier :

    ls Cas*`
    Cast3M-2009-linux32.tar.gz

Ensuite décompresser l'archiver avec :

`tar -xvzf Cast3M-2009-linux32.tar.gz`

déplacez le dossier décompressé dans /opt/

`sudo mv castem_V09/ /opt/`

procédez à la suite de l'installation

`cd /opt/castem_V09/export CASTEM=/opt/castem_V09chmod a+x install_exe./install_exe`

Si l'installation se termine bien, le message suivant apparaitra

    L'installation semble reussie.
    Fin normale de install

Configuration du terminal
-------------------------

`cd ~nano .bashrc`

ajouter ces deux lignes puis enregistrez

    export CASTEM=/opt/castem\_V09  
    export PATH=\$PATH:/opt/castem\_V09/bin

Lancement
---------

Pour lancer mon\_fichier.dgibi par exemple, exécuter la commande

`castem09 mon_fichier.dgibi`

Démonstration
-------------

Une petite vidéo pour voir ce que l'on peut faire avec ce logiciel, c'est très impressionnant.

[demo\_castem](./public/2009/demo_castem.avi)

</p>

