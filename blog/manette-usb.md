Title: Manette USB  
Date: 2011-11-19 23:50  
Category: Matériel  
Tags:  

Slug: manette-usb  
Status: published  

![Bigben Interactive Artikelbild](./public/2011/BB_251531_01.jpg "Bigben Interactive Artikelbild, nov. 2011")

Je présente ici une manette qui se branche en USB et que j'ai testé sous Ubuntu, elle est directement reconnue.La seule manip à faire est de configurer le jeu (Snes9x, SuperTuxKart, ...) pour associer les touches avec les bonnes actions.

La manette est de la marque Big Ben. Sa référence est BB4290, un lsusb renvoie les infos suivantes

`Bus 005 Device 003: ID 054c:0268 Sony Corp. Batoh Device / PlayStation 3 Controller`

</p>

