Title: Futjitsu ESPRIMO P3520 E85+  
Date: 2018-09-23 11:22  
Category: Matériel  
Tags:  

Slug: futjitsu-esprimo-p3520-e85  
Status: published  

Voilà un pc que je viens d'acheter un peu par hasard, ce pc était en vente sous leboncoin à coté de chez moi et vu le prix (40€), j'ai sauté sur l'occasion.

Le pc est un Futjitsu ESPRIMO P3520 E85+, je viens d'y installer xubuntu 18.04.1 sans soucis particulier. J'ai uniquement remplacé le disque dur de 160 Go par un de 500 Go.

![esprimo\_im\_mini](./public/2018/fujitsu_siemens_p2560_mini.jpg "esprimo_im_mini, sept. 2018")

C'est un pc sorti il y a quelques années maintenant, ce n'est donc pas une foudre de guerre mais un pc tout a fait utilisable pour de la bureautique, internet même si je réfléchis à en faire un serveur.

Les principales caractéristiques matérielles sont :

-   Intel Core 2 Duo processor E8400 (2C/2T, 3.0 GHz, 6 MB, 1333 MHz)
-   2 x 2 GB DDR2, 800 MHz, PC2-6400, DIMM
-   SATA II, 7200 rpm, 500 GB, 3.5-inch, S.M.A.R.T.
-   NVIDIA GeForce 9300GE, 256 MB
-   Carte mère μATX D2841

Pour plus d'infos, la doc technique est disponible [ici](./public/2018/ds-esprimo-p3520-e85.pdf).Le retour de commande usuelle pour lister le matériel en place est dispo [ici](./public/2018/lshw.txt)

</p>

